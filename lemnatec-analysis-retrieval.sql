CREATE OR REPLACE FUNCTION create_metadata_view() RETURNS void LANGUAGE plpgsql AS $$
DECLARE
    list text;
BEGIN
  DROP VIEW IF EXISTS metadata_view;
  DROP VIEW IF EXISTS raw_metadata_view;
  CREATE MATERIALIZED VIEW raw_metadata_view AS
  SELECT DISTINCT id_tag,
      name,
      value
  FROM r2_objcc_meta
  LEFT JOIN r2_objcc on r2_objcc.id = r2_objcc_meta.r2_objcc_id
  LEFT JOIN analysis on analysis.id = r2_objcc.analysis_id
  LEFT JOIN snapshot on snapshot.id = analysis.snapshot_id;
       
  SELECT string_agg(format('jdata->>%1$L "%1$s"', name), ', ')
  FROM
    ( SELECT DISTINCT name
     FROM raw_metadata_view ) sub INTO list;
   
  EXECUTE format($f$
  CREATE MATERIALIZED VIEW metadata_view AS
  SELECT id_tag,
         %s
  FROM
    ( SELECT id_tag,
       json_object_agg(name, value) jdata
     FROM raw_metadata_view
     GROUP BY 1
     ORDER BY 1 ) sub $f$,
          list);
END $$;

CREATE OR REPLACE FUNCTION create_path_view() RETURNS void LANGUAGE plpgsql AS $$
DECLARE
    list text;
BEGIN
  DROP VIEW IF EXISTS path_view;
  DROP VIEW IF EXISTS raw_path_view;
  CREATE MATERIALIZED VIEW raw_path_view AS
  SELECT snapshot_id ,
      camera_label,
         path
        FROM image_file_table
        JOIN tile ON tile.image_oid = image_file_table.id
        JOIN tiled_image ON tiled_image_id = tiled_image.id
        JOIN snapshot ON tiled_image.snapshot_id = snapshot.id;
        
  SELECT string_agg(format('jdata->>%1$L "%1$s"',camera_label), ', ')
  FROM
    ( SELECT DISTINCT camera_label
     FROM raw_path_view ) sub INTO list;
   
  EXECUTE format($f$
  CREATE MATERIALIZED VIEW path_view AS
  SELECT snapshot_id,
         %s
  FROM
    ( SELECT snapshot_id,
       json_object_agg(camera_label, path) jdata
     FROM raw_path_view
     GROUP BY 1
     ORDER BY 1 ) sub $f$,
          list);
END $$;


CREATE OR REPLACE FUNCTION build_writer_label_view(writer_label text) RETURNS void LANGUAGE plpgsql AS
$$
DECLARE
  query text;
BEGIN

query:= format( 'DROP VIEW IF EXISTS %1$s_analysis;
  CREATE MATERIALIZED VIEW %1$s_analysis AS
  WITH Senescence AS (SELECT cc_area_abs, 
  cc_area_rel, 
  r2_objcc_id 
  FROM r2_objcc_objovcc
  WHERE cc_name = ''Senescent''),

  Healthy AS (SELECT cc_area_abs, 
  cc_area_rel, 
  r2_objcc_id 
  FROM r2_objcc_objovcc WHERE cc_name = ''Healthy'')
  SELECT distinct on (snapshot_id)
    writer_label AS "%1$s Writer Label",
            area AS "%1$s Area",
            Senescence.cc_area_abs AS "%1$s Senescent_ColorClassAreaAbsolut",
            Senescence.cc_area_rel AS "%1$s Senescent_ColorClassAreaRelative",
            Healthy.cc_area_abs AS "%1$s Healthy_ColorClassAreaAbsolut",
            Healthy.cc_area_rel AS "%1$s Healthy_ColorClassAreaRelative",
            "2nd_mom_pa_abs_lg" AS "%1$s 2nd Moment Principle Axis Large Abs",
            "2nd_mom_pa_abs_sm" AS "%1$s 2nd Moment Principle Axis Small Abs",
            "2nd_mom_pa_norm_lg" AS "%1$s 2nd Moment Principle Axis Large Norm",
            "2nd_mom_pa_norm_sm" AS "%1$s 2nd Moment Principle Axis Small Norm",
            "2nd_mom_pa_ratio" AS "%1$s 2nd Moments Principal Axis Ratio",
            area_distri_above_abs AS "%1$s Area Distribution Above Absolut",
            area_distri_above_rel AS "%1$s Area Distribution Above Relative",
            area_distri_below_abs AS "%1$s Area Distribution Below Absolut",
            area_distri_below_rel AS "%1$s Area Distribution Below Relative",
            bd_pnt_cnt AS "%1$s Boundary Point Count",
            bd_pnt_rdns "%1$s Boundary Point Roundness",
            bd_pnt_size_ratio AS "%1$s Boundary Points To Area Ratio",
            caliper AS "%1$s Caliper Length",
            circumference AS "%1$s Circumference",
            com_border_dist AS "%1$s Center Of Mass To Boundary Distance",
            com_x AS "%1$s Center Of Mass X",
            com_y AS "%1$s Center Of Mass Y",
            compactness AS "%1$s Compactness",
            convex_hull_area AS "%1$s Convex Hull Area",
            convex_hull_circ AS "%1$s Convex Hull Circumference",
            excentr AS "%1$s Excentricity",
            extent_x AS "%1$s Object Extent X",
            extent_y AS "%1$s Object Extent Y",
            mdist_hline_above AS "%1$s Max Dist Above Horizon Line",
            mdist_hline_below AS "%1$s Max Dist Below Horizon Line",
            mean_color_b AS "%1$s Mean Hue",
            mean_color_b_sigma AS "%1$s Mean Hue Variance",
            min_circle_dia AS "%1$s Enclosing Circle Diameter",
            min_rect_area AS "%1$s Min Area Rectangle Area",
            rdns AS "%1$s Roundness",
            z_rotmom_abs AS "%1$s Absolut Z Rotation 2nd Moment",
            z_rotmom_norm AS "%1$s Normalised Z Rotation 2nd Moment",
            analysis_id AS "%1$s_analysis_id",
            snapshot_id AS "%1$s_snapshot_id",
            label AS "%1$s_analysis_label"
        FROM r2_objcc
        LEFT JOIN analysis on r2_objcc.analysis_id = analysis.id
        LEFT JOIN Senescence on Senescence.r2_objcc_id = r2_objcc.id
        LEFT JOIN Healthy on Healthy.r2_objcc_id = r2_objcc.id
        WHERE writer_label = ''%1$s''
        ORDER BY snapshot_id asc, analysis_id desc',writer_label);

          EXECUTE query;
END
$$;

DO $$ BEGIN
  PERFORM create_metadata_view();
  PERFORM create_path_view();
  PERFORM build_writer_label_view(writer_label) from (select distinct writer_label FROM r2_objcc) as t;
END $$;


CREATE INDEX index_metadata_view_on_id_tag ON metadata_view(id_tag);
CREATE INDEX index_snapshot_on_id_tag ON snapshot(id_tag);
CREATE INDEX index_path_view_on_snapshot_id ON path_view(snapshot_id);
CREATE INDEX index_rgb_sv1_analysis_on_snapshot_id ON rgb_sv1_analysis("RGB_SV1_snapshot_id");
CREATE INDEX index_rgb_sv2_analysis_on_snapshot_id ON rgb_sv2_analysis("RGB_SV2_snapshot_id");
CREATE INDEX index_rgb_tv_analysis_on_snapshot_id ON rgb_tv_analysis("RGB_TV_snapshot_id");

--select table_name from INFORMATION_SCHEMA.views where table_name like '%analysis';

SELECT (regexp_replace(COALESCE(snapshot.id_tag, '0'), '[^0-9]+', '', 'g') || floor(extract(epoch from time_stamp)))::bigint AS "id",
    snapshot.id_tag AS "Snapshot ID Tag",
    snapshot.time_stamp as "Snapshot Time Stamp",
    snapshot.measurement_label,
    metadata_view.*,
    --DATE_PART('day', snapshot.time_stamp - '2017-02-02') as "Imaging Day",
    snapshot.water_amount AS "Water Amount",
    snapshot.weight_after AS "Weight After",
    snapshot.weight_before AS "Weight Before",
    rgb_sv1_analysis."RGB_SV1 Area" + rgb_sv2_analysis."RGB_SV2 Area" + rgb_tv_analysis."RGB_TV Area" AS "Projected Shoot Area [pixel]",
    rgb_sv1_analysis.*,
    rgb_sv2_analysis.*,
    rgb_tv_analysis.*,
    path_view.*
  FROM snapshot
  LEFT JOIN metadata_view ON snapshot.id_tag = metadata_view.id_tag 
  RIGHT JOIN path_view ON path_view.snapshot_id = snapshot.id
  LEFT JOIN rgb_sv1_analysis ON rgb_sv1_analysis."RGB_SV1_snapshot_id" = snapshot.id
  LEFT JOIN rgb_sv2_analysis ON rgb_sv2_analysis."RGB_SV2_snapshot_id" = snapshot.id
  LEFT JOIN rgb_tv_analysis ON rgb_tv_analysis."RGB_TV_snapshot_id" = snapshot.id
  WHERE snapshot.id_tag ~ '^\d+ ?'
  AND snapshot.measurement_label LIKE '038%'
  ORDER BY "Snapshot ID Tag",
     time_stamp